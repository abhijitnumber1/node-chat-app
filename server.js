var express = require('express');
var bodyPareser = require('body-parser');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http)
var mongoose = require('mongoose');

app.use(express.static(__dirname));
app.use(bodyPareser.json());
app.use(bodyPareser.urlencoded({extended:false}));

var dbUrl = 'mongodb://<user>:<user123>@ds131258.mlab.com:31258/nodelearning';

var messages = [
    {name:"Abhijit",message:"Hi"},
    {name:"Pradyut",message:"Hello"}
]

app.get('/messages',(req,res)=>{
    res.send(messages);
})

app.post('/messages',(req,res)=>{
    messages.push(req.body);
    io.emit('message',req.body);
    res.sendStatus(200)
})

io.on('connection',(socket)=>{
    console.log('A user is connected')
})

mongoose.connect('mongodb://user:user1234@ds131258.mlab.com:31258/nodelearning', 
    {useNewUrlParser: true },(err)=>{
        if(err) {
            console.log('Some problem with the connection ' +err);
        } else {
            console.log('The Mongoose connection is ready');
        }
    });

var server = http.listen(3000,()=>{
    console.log('Server is listning on port: ',server.address().port);
});